# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [1.0.0] - 2021-05-26
- This is a major release which breaks backwards compatibility since the package was renamed from `sinfo` to `session_info` and some defaults changed.

## Added
- Clearer examples in the README.

## Changed
- The package name was changed to `session_info` to make it more discoverable and self-explanatory.
    - The `sinfo` PyPI package will be kept around to avoid breaking old installs and you can downgrade to 0.3.2 if you want to use it without seeing this message. For the latest features and bug fixes, please install `session_info` instead.
- The syntax for the default usage changed from `from sinfo import sinfo; sinfo()` to `import session_info; session_info.show()`.
- The default is now to show the HTML version in notebooks and to not write a requirement text file unless explicitly asked for.
- The CPU info is excluded by default.

## [0.3.4] - 2021-05-26
### Added
- Inform about package name change in the README.

## [0.3.3] - 2021-05-26
### Added
- Inform about package name change when called.

## [0.3.2] - 2021-05-06

### Fixed
- Do not print `None` for output fields that are not included in the output
- Show only dependencies that are not already in the imported modules
- Deal gracefully with version functions that do not return version strings
- Use a single line for description in setup.py to avoid some installs breaking

## [0.3.1] - 2020-01-15

### Changed
- HTML defaults to False for output to show up on GitHub.

## [0.3.0] - 2019-12-14

### Added
- Support for tuples version attributes.
- Print sinfo version by default.
- Output as concealed HTML for notebooks.
- Detect notebook name and prefix requirements file accordingly.
- Docstring for requirements text file parameters.
- Detect desirable behavior when parameters are set to `None`.
    - For html, jupyter, and requirements file.

### Changed
- Parameters are no longer prefaced with `print_`.

### Fixed
- Remove duplicate IPython output.
