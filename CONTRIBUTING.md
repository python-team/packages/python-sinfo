Thanks for your interest in helping to improve `session_info`!

The best way to get in touch to discuss ideas is to [open a new
issue](https://gitlab.com/joelostblom/session_info/issues).

Remember to follow the [Code of
conduct](https://gitlab.com/joelostblom/session_info/blob/master/CODE_OF_CONDUCT.md)
when you participate in this project =)
